import { Story, Meta } from "@storybook/react/types-6-0";
import { BasketTotalRow, BasketTotalRowProps } from "./BasketTotalRow";

export default {
  title: "Atoms/BasketTotalRow",
  component: BasketTotalRow,
} as Meta;

const Template: Story<BasketTotalRowProps> = (args) => (
  <BasketTotalRow {...args} />
);

export const Subtotal = Template.bind({});
Subtotal.args = {
  title: "Subtotal",
  amount: "200",
};

export const Vat = Template.bind({});
Vat.args = {
  title: "VAT @ 20%",
  amount: "50",
};

export const Total = Template.bind({});
Total.args = {
  title: "Total",
  amount: "250",
};
