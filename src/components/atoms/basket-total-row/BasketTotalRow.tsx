import React from "react";
import { BasketTotalRowContainer } from "./BasketTotalRow.styles";

export interface BasketTotalRowProps {
  title: string;
  amount: string;
}

export const BasketTotalRow: React.FC<BasketTotalRowProps> = ({
  title,
  amount,
}): JSX.Element => {
  return (
    <BasketTotalRowContainer>
      <div data-testid="title">{title}</div>
      <div data-testid="amount">£{amount}</div>
    </BasketTotalRowContainer>
  );
};
