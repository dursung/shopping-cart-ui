import styled from "styled-components";

export const BasketTotalRowContainer = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 10px;
`;
