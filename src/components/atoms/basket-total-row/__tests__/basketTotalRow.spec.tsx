import { render, screen } from "@testing-library/react";
import { BasketTotalRow } from "../BasketTotalRow";

describe("<BasketTotalRow/>", () => {
  test("renders the component correctly", () => {
    render(<BasketTotalRow title="Total" amount="100" />);

    expect(screen.getByTestId("title").textContent).toBe("Total");
  });
});
