import { Story, Meta } from "@storybook/react/types-6-0";
import { Input, InputProps } from "./Input";
import { action } from "@storybook/addon-actions";

export default {
  title: "Atoms/Input",
  component: Input,
} as Meta;

const Template: Story<InputProps> = (args) => <Input {...args} />;

export const Default = Template.bind({});
Default.args = {
  value: 3,
  onChange: action("clicked"),
};
