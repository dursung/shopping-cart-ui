import React from "react";

export interface InputProps {
  value: number;
  forwardedRef: React.LegacyRef<HTMLInputElement> | undefined;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  checkQuantityValue?: (e: React.FocusEvent<HTMLInputElement>) => void;
}

export const Input: React.FC<InputProps> = ({
  value,
  forwardedRef,
  onChange,
  checkQuantityValue,
  ...props
}): JSX.Element => {
  return (
    <input
      min="1"
      max="10"
      type="number"
      ref={forwardedRef}
      value={value}
      onChange={onChange}
      onBlur={checkQuantityValue}
      style={{
        border: "1px solid ",
        borderRadius: "2px",
        width: "40px",
        textAlign: "center",
      }}
      {...props}
    />
  );
};
