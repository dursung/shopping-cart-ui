import React from "react";
import { BasketHeaderH1 } from "./BasketHeader.styles";

export interface BasketHeaderProps {
  header: string;
}

export const BasketHeader: React.FC<BasketHeaderProps> = ({
  header,
  ...props
}): JSX.Element => {
  return (
    <div>
      <BasketHeaderH1 data-testid="header">{header}</BasketHeaderH1>
    </div>
  );
};
