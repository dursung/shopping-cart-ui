import styled from "styled-components";

export const BasketHeaderH1 = styled.h1`
  border-bottom: 1px;
  text-align: center;
  padding-bottom: 20px;
  font-size: 2em;
  font-weight: 500;
`;
