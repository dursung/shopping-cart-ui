import { render, screen } from "@testing-library/react";
import { BasketHeader } from "../BasketHeader";

describe("<BasketHeader/>", () => {
  test("renders the component correctly", () => {
    render(<BasketHeader header="Header" />);

    expect(screen.getByTestId("header").textContent).toBe("Header");
  });
});
