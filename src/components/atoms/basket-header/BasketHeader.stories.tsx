import { Story, Meta } from "@storybook/react/types-6-0";
import { BasketHeader, BasketHeaderProps } from "./BasketHeader";

export default {
  title: "Atoms/BasketHeader",
  component: BasketHeader,
} as Meta;

const Template: Story<BasketHeaderProps> = (args) => <BasketHeader {...args} />;

export const Default = Template.bind({});
Default.args = {
  header: "Review Your Order & Complete Checkout",
};
