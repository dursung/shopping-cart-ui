import React, { CSSProperties } from "react";
import { StyledButton } from "./Button.styles";

export interface ButtonProps {
  style: CSSProperties | undefined;
  label: string;
  disabled?: boolean;
  onClick?: (e: React.MouseEvent<HTMLElement>) => void;
}

export const Button: React.FC<ButtonProps> = ({
  style,
  label,
  disabled,
  onClick,
  ...props
}): JSX.Element => {
  return (
    <StyledButton
      type="button"
      className="button"
      style={style}
      onClick={onClick}
      disabled={disabled}
      data-testid={`button-${label}`}
      {...props}
    >
      {label}
    </StyledButton>
  );
};
