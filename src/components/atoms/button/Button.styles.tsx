import styled from "styled-components";

export const StyledButton = styled.button`
  border: none;
  color: #ffffff;
  border-radius: 3px;
  cursor: pointer;
  padding: 5px 5px;
  font-weight: 600;
  margin-left: 3px;
`;
