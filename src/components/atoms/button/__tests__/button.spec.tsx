import { render, fireEvent, waitFor, screen } from "@testing-library/react";
import { Button } from "../Button";

describe("<Button/>", () => {
  test("renders the component correctly", () => {
    render(<Button label="Click" />);

    expect(screen.getByTestId("button-Click").textContent).toBe("Click");
  });

  test("click the button", async () => {
    render(<Button label="Click" onClick={() => {}} />);

    fireEvent.click(screen.getByText("Click"));

    await waitFor(() => screen.getByTestId("button-Click"));

    expect(screen.getByTestId("button-Click").textContent).toBe("Click");
  });
});
