import { Story, Meta } from "@storybook/react/types-6-0";
import { Button, ButtonProps } from "./Button";

export default {
  title: "Atoms/Button",
  component: Button,
  argTypes: {
    backgroundColor: { control: "color" },
  },
} as Meta;

const Template: Story<ButtonProps> = (args) => <Button {...args} />;

export const MinusButton = Template.bind({});
MinusButton.args = {
  label: "-",
};

export const PlusButton = Template.bind({});
PlusButton.args = {
  label: "+",
};

export const DeleteButton = Template.bind({});
DeleteButton.args = {
  label: "Delete",
};
