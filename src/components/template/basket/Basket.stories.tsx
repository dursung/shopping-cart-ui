import { Story, Meta } from "@storybook/react/types-6-0";
import { Basket } from "./Basket";

export default {
  title: "Template/Basket",
  component: Basket,
} as Meta;

const Template: Story = (args) => <Basket {...args} />;

export const Default = Template.bind({});
Default.args = {};
