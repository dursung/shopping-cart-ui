import React, { useContext, useEffect, useState } from "react";
import { BasketContext } from "../../../context";
import { BasketHeader } from "../../atoms/basket-header/BasketHeader";
import { BasketTable } from "../../organisms/basket-table/BasketTable";
import { BasketTotal } from "../../organisms/basket-total/BasketTotal";

import "./Basket.css";

export const Basket: React.FC = (): JSX.Element => {
  const { products, updateProducts } = useContext(BasketContext);
  const [subTotal, setSubTotal] = useState<number>(0);
  const [vat, setVat] = useState<number>(0);

  const submitBasket = () => {
    // TODO: call rest API to submit the basket
    if (products.length > 0) alert("Submitted");
  };

  useEffect(() => {
    if (products.length > 0) {
      const total = products
        .map((product) => product.price * product.quantity)
        .reduce((sum, current) => sum + (isNaN(current) ? 0 : current));
      setSubTotal(total);
      setVat(total * 0.2);
    } else {
      setSubTotal(0);
      setVat(0);
    }
  }, [products]);

  return (
    <div className="container">
      <BasketHeader header="Review Your Order & Complete Checkout" />
      <hr />
      <BasketTable products={products} updateProducts={updateProducts} />
      <BasketTotal subTotal={subTotal} vat={vat} onSubmit={submitBasket} />
    </div>
  );
};
