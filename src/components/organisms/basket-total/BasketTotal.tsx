import React from "react";
import { BasketTotalRow } from "../../atoms/basket-total-row/BasketTotalRow";
import { Button } from "../../atoms/button/Button";
import { BasketTotalSection } from "../../molecules/basket-total-section/BasketTotalSection";
import {
  BasketTotalContainer,
  StyledBasketTotal,
  BasketBuyButtonContainer,
} from "./BasketTotal.styles";
export interface BasketTotalProps {
  subTotal: number;
  vat: number;
  onSubmit: () => void;
}

export const BasketTotal: React.FC<BasketTotalProps> = ({
  subTotal,
  vat,
  onSubmit,
}): JSX.Element => {
  return (
    <BasketTotalContainer>
      <div>
        <BasketTotalSection
          subTotal={subTotal.toFixed(2).toString()}
          vat={vat.toFixed(2).toString()}
        />
      </div>
      <StyledBasketTotal>
        <BasketTotalRow title="Total" amount={(subTotal + vat).toFixed(2)} />
      </StyledBasketTotal>
      <BasketBuyButtonContainer>
        <Button
          label="Buy Now"
          style={{
            backgroundColor: "#13B8B8",
            width: "150px",
            height: "50px",
            borderRadius: "100px",
          }}
          disabled={subTotal === 0}
          onClick={onSubmit}
        />
      </BasketBuyButtonContainer>
    </BasketTotalContainer>
  );
};
