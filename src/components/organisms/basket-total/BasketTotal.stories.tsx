import { Story, Meta } from "@storybook/react/types-6-0";
import { BasketTotal, BasketTotalProps } from "./BasketTotal";

export default {
  title: "Organisms/BasketTotal",
  component: BasketTotal,
} as Meta;

const Template: Story<BasketTotalProps> = (args) => <BasketTotal {...args} />;

export const Default = Template.bind({});
Default.args = {
  subTotal: 200,
  vat: 50,
};
