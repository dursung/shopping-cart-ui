import styled from "styled-components";

export const BasketTotalContainer = styled.div`
  padding-top: 20px;
`;

export const StyledBasketTotal = styled.div`
  border-top: 1px solid var(--accent);
  padding: 20px 0;
  font-weight: bold;
  font-size: 20px;
`;

export const BasketBuyButtonContainer = styled.div`
  border-top: 1px solid var(--accent);
  padding: 20px 0;
  display: flex;
  justify-content: right;
`;
