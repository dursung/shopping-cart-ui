import { Story, Meta } from "@storybook/react/types-6-0";
import { BasketTable, BasketTableProps } from "./BasketTable";
import { Product } from "../../../models/Product";

export default {
  title: "Organisms/Basket Table",
  component: BasketTable,
} as Meta;

const Template: Story<BasketTableProps> = (args) => <BasketTable {...args} />;

export const Default = Template.bind({});
const products: Product[] = [
  { id: 0, name: "Apple", price: 0.52, quantity: 2 },
  { id: 1, name: "Banana", price: 0.67, quantity: 3 },
];

Default.args = {
  products,
};
