import React from "react";
import { Product } from "../../../models/Product";
import { Button } from "../../atoms/button/Button";
import { QuantityInput } from "../../molecules/quantity-input/QuantityInput";
import "./BasketTable.css";

export interface BasketTableProps {
  products: Product[];
  updateProducts: (products: Product[]) => void;
}

export const BasketTable: React.FC<BasketTableProps> = ({
  products,
  updateProducts,
}): JSX.Element => {
  const valueChange = (e: any, inputRef: React.RefObject<any>) => {
    const newQuantity = !inputRef
      ? parseInt(e.target.value)
      : parseInt(inputRef.current.value) + parseInt(e.target.dataset.value);
    if (newQuantity > 10 || newQuantity === 0) return;
    updateQuantity(newQuantity, e.target.dataset.productId);
  };

  const updateQuantity = (newQuantity: number, id: string) => {
    const updatedProducts = products.map((product) => {
      if (product.id === parseInt(id)) {
        return {
          ...product,
          quantity: newQuantity | 0,
        };
      } else {
        return { ...product };
      }
    });
    updateProducts(updatedProducts);
  };

  const checkQuantityValue = (e: React.ChangeEvent<HTMLInputElement>) => {
    let newQuantity = parseInt(e.target.value);
    if (isNaN(newQuantity)) newQuantity = 1;
    updateQuantity(newQuantity, e.target.dataset["productId"] as string);
  };

  const deleteItem = (e: React.MouseEvent<HTMLElement>) => {
    const target = e.target as HTMLElement;
    const filteredProducts = products.filter(
      (product) =>
        product.id !== parseInt(target.dataset["productId"] as string)
    );
    updateProducts(filteredProducts);
  };

  return (
    <div className="basket_table_container">
      <h2 className="basket_table_header">Review Your Order</h2>
      <div className="table_container">
        <table className="basket_table">
          <thead>
            <tr>
              <th>Product</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Cost</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {products.map((product) => (
              <tr key={product.name}>
                <td>{product.name}</td>
                <td>£{product.price}</td>
                <td>
                  <QuantityInput
                    productId={product.id}
                    quantity={product.quantity}
                    valueChange={valueChange}
                    checkQuantityValue={checkQuantityValue}
                  />
                </td>
                <td>£{(product.price * product.quantity).toFixed(2)}</td>
                <td>
                  <Button
                    label="Delete"
                    data-product-id={product.id}
                    style={{ backgroundColor: "red" }}
                    onClick={deleteItem}
                  />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};
