import { Story, Meta } from "@storybook/react/types-6-0";
import {
  BasketTotalSection,
  BasketTotalSectionProps,
} from "./BasketTotalSection";

export default {
  title: "Molecules/BasketTotalSection",
  component: BasketTotalSection,
} as Meta;

const Template: Story<BasketTotalSectionProps> = (args) => (
  <BasketTotalSection {...args} />
);

export const Default = Template.bind({});
Default.args = {
  subTotal: "200",
  vat: "50",
};
