import styled from "styled-components";

export const BasketTotalSectionContainer = styled.div`
  border-top: 1px solid var(--accent);
  padding: 20px 0;
  color: #b3bcc2;
  font-weight: bold;
  font-size: 18px;
`;
