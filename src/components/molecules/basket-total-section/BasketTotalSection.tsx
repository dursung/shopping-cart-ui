import { BasketTotalRow } from "../../atoms/basket-total-row/BasketTotalRow";
import { BasketTotalSectionContainer } from "./BasketTotalSection.styles";

export interface BasketTotalSectionProps {
  subTotal: string;
  vat: string;
}

export const BasketTotalSection: React.FC<BasketTotalSectionProps> = ({
  subTotal,
  vat,
}): JSX.Element => {
  return (
    <BasketTotalSectionContainer>
      <BasketTotalRow title="Subtotal" amount={subTotal} />
      <BasketTotalRow title="VAT @ 20%" amount={vat} />
    </BasketTotalSectionContainer>
  );
};
