import { Story, Meta } from "@storybook/react/types-6-0";
import { QuantityInput, QuantityInputProps } from "./QuantityInput";
import { action } from "@storybook/addon-actions";

export default {
  title: "Molecules/QuantityInput",
  component: QuantityInput,
} as Meta;

const Template: Story<QuantityInputProps> = (args) => (
  <QuantityInput {...args} />
);

export const Default = Template.bind({});
const valueChange = () => {};
Default.args = {
  quantity: 3,
  valueChange,
};
