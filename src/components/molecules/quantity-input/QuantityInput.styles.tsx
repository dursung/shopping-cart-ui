import styled from "styled-components";

export const QuantityContainer = styled.div`
  display: flex;
`;

export const ButtonsContainer = styled.div`
  margin-left: 10px;
`;
