import { useRef } from "react";
import { Button } from "../../atoms/button/Button";
import { Input } from "../../atoms/input/Input";
import { QuantityContainer, ButtonsContainer } from "./QuantityInput.styles";

export interface QuantityInputProps {
  quantity: number;
  productId: number;
  valueChange: any;
  checkQuantityValue?: (e: any) => void;
}

export const QuantityInput: React.FC<QuantityInputProps> = ({
  quantity,
  productId,
  valueChange,
  checkQuantityValue,
}): JSX.Element => {
  const inputRef = useRef(null);
  return (
    <QuantityContainer>
      <Input
        value={quantity}
        onChange={valueChange}
        forwardedRef={inputRef}
        data-product-id={productId}
        checkQuantityValue={checkQuantityValue}
      />
      <ButtonsContainer>
        <Button
          style={{ backgroundColor: "orange", width: "30px" }}
          label="-"
          data-value="-1"
          data-product-id={productId}
          onClick={(event) => valueChange(event, inputRef)}
        />
        <Button
          style={{ backgroundColor: "#15BCAB", width: "30px" }}
          label="+"
          data-value="1"
          data-product-id={productId}
          onClick={(event) => valueChange(event, inputRef)}
        />
      </ButtonsContainer>
    </QuantityContainer>
  );
};
