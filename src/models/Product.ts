export interface Product {
  id: number;
  name: string;
  price: number;
  quantity: number;
}

export interface InputChangeInterface {
  target: {
    value: string;
    dataset: {
      productId: string;
      value: string;
    };
  };
}
