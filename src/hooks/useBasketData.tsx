import { useMutation } from "react-query";
import axios from "axios";
import { Product } from "../models/Product";

const addBasket = (products: Product[]) => {
  return axios.post("http://localhost:4000/basket/products", products);
};

export const useBasketData = (products: Product[]) => {
  return useMutation(addBasket);
};
