import React, { FC, useState } from "react";
import { Product } from "../models/Product";
import { BasketContextState } from "./types";

const contextDefaultValues: BasketContextState = {
  products: [
    { id: 0, name: "Apple", price: 0.52, quantity: 2 },
    { id: 1, name: "Banana", price: 0.67, quantity: 3 },
  ],
  updateProducts: () => {},
};

export const BasketContext = React.createContext<BasketContextState>(
  contextDefaultValues
);

const BasketProvider: FC = ({ children }) => {
  const [products, setProducts] = useState<Product[]>(
    contextDefaultValues.products
  );

  const updateProducts = (products: Product[]) => setProducts(products);

  return (
    <BasketContext.Provider value={{ products, updateProducts }}>
      {children}
    </BasketContext.Provider>
  );
};

export default BasketProvider;
