import { Product } from "../models/Product";

export type BasketContextState = {
  products: Product[];
  updateProducts: (products: Product[]) => void;
};
