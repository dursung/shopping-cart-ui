import "./App.css";
import { Basket } from "./components/template/basket/Basket";
import ContextProvider from "./context";

function App() {
  return (
    <ContextProvider>
      <Basket />
    </ContextProvider>
  );
}

export default App;
